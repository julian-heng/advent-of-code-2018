#!/usr/bin/env python3

import re
from collections import defaultdict

def part1(lines):
    coords = dict()
    size = dict()
    infinite = set()

    max_x = 0
    max_y = 0
    id = 0
    for line in lines:
        x, y = map(int, re.findall("\d+", line))
        max_x = x if x > max_x else max_x
        max_y = y if y > max_y else max_y
        coords[id] = (x, y)
        id += 1

    for i in range(max_x + 1):
        for j in range(max_y + 1):
            dists = [(abs(x - i) + abs(y - j), id) for id, (x, y) in coords.items()]

            dists.sort()
            if len(dists) == 1 or dists[0][0] != dists[1][0]:
                id = dists[0][1]
                size.setdefault(id, 0)
                size[id] += 1

                if i in [0, max_x] or j in [0, max_y]:
                    infinite.add(id)

    max_area = max([area for id, area in size.items() if id not in infinite])
    print("Maximum area: {}".format(max_area))

    return coords, max_x, max_y

def part2(coords, max_x, max_y):
    area = 0

    for i in range(max_x + 1):
        for j in range(max_y + 1):
            dists = [abs(x - i) + abs(y - j) for (x, y) in coords.values()]
            total_dist = sum(dists)
            if total_dist < 10000:
                area += 1

    print("Area of safe region: {}".format(area))

def main():
    with open("input") as f:
        file = f.readlines()

    coords, max_x, max_y = part1(file)
    part2(coords, max_x, max_y)

main()
