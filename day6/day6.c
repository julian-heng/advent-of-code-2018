#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#define FALSE 0
#define TRUE !FALSE

typedef struct coordinates
{
    int x, y;
} coordinates;

void prep(char **arr, int n, coordinates **coords, int *max_x, int *max_y);
void part1(coordinates *coords, int n, int max_x, int max_y);
void part2(coordinates *coords, int n, int max_x, int max_y);
void read_file(char ***arr, int *num_lines, int *num_length);
int min(int *arr, int n);

int main(void)
{
    char **file = NULL;
    int file_lines = 0, file_length = 0, max_x = 0, max_y = 0, i;
    coordinates *coords = NULL;

    read_file(&file, &file_lines, &file_length);

    if (file)
    {
        prep(file, file_lines, &coords, &max_x, &max_y);
        part1(coords, file_lines, max_x, max_y);
        part2(coords, file_lines, max_x, max_y);

        free(coords);
        coords = NULL;

        for (i = 0; i < file_lines; i++)
        {
            free(file[i]);
            file[i] = NULL;
        }

        free(file);
        file = NULL;
    }

    return 0;
}

void prep(char **arr, int n, coordinates **coords, int *max_x, int *max_y)
{
    int i;

    (*coords) = (coordinates*)malloc(n * sizeof(coordinates));
    for (i = 0; i < n; i++)
    {
        sscanf(arr[i], "%d, %d", &((*coords)[i].x), &((*coords)[i].y));
        *max_x = (*coords)[i].x > *max_x ? (*coords)[i].x : *max_x;
        *max_y = (*coords)[i].y > *max_y ? (*coords)[i].y : *max_y;
    }
}

void part1(coordinates *coords, int n, int max_x, int max_y)
{
    int i, j, k, max_area, min_dist, count, index;
    int *size = NULL, *infinite = NULL, *dists = NULL;

    size = (int*)malloc(n * sizeof(int));
    infinite = (int*)malloc(n * sizeof(int));
    dists = (int*)malloc(n * sizeof(int));

    memset(size, 0, n * sizeof(int));
    memset(infinite, FALSE, n * sizeof(int));
    memset(dists, 0, n * sizeof(int));

    max_area = 0;
    index = 0;

    for (i = 0; i < max_x + 1; i++)
        for (j = 0; j < max_y + 1; j++)
        {
            memset(dists, 0, n * sizeof(int));

            min_dist = 0;
            count = 0;
            index = 0;

            for (k = 0; k < n; k++)
                dists[k] = abs(i - coords[k].x) + abs(j - coords[k].y);

            min_dist = min(dists, n);

            for (k = 0; k < n; k++)
                if (min_dist == dists[k])
                {
                    count++;
                    index = k;
                }

            if (count == 1)
            {
                size[index]++;

                if ((! infinite[index]) &&
                   ((i == 0 || i == max_x || j == 0 || j == max_y)))
                    infinite[index] = TRUE;
            }
        }

    for (i = 0; i < n; i++)
        max_area = (! infinite[i] && size[i] > max_area) ? size[i] : max_area;

    printf("Maximum area: %d\n", max_area);

    free(size);
    free(infinite);
    free(dists);

    size = NULL;
    infinite = NULL;
    dists = NULL;
}

void part2(coordinates *coords, int n, int max_x, int max_y)
{
    int i, j, k, area = 0, total_dist = 0;

    for (i = 0; i < max_x + 1; i++)
        for (j = 0; j < max_y + 1; j++)
        {
            total_dist = 0;
            for (k = 0; k < n; k++)
                total_dist += abs(i - coords[k].x) + abs(j - coords[k].y);
            area += total_dist < 10000 ? 1 : 0;
        }

    printf("Area of safe region: %d\n", area);
}

void read_file(char ***arr, int *num_lines, int *num_length)
{
    FILE *f = fopen("./input", "r");
    int ch, i, length = 0;

    if (ferror(f))
        return;

    *num_lines = 0;
    while ((ch = fgetc(f)) != EOF && ! ferror(f))
        if (ch == '\n')
        {
            *num_lines += 1;
            *num_length = length > *num_length ? length : *num_length;
            length = 0;
        }
        else
            length++;
    fseek(f, 0, SEEK_SET);

    if (*num_lines == 0) (*num_lines)++;
    (*num_length)++;

    (*arr) = (char**)malloc(*num_lines * sizeof(char*));
    for (i = 0; i < *num_lines; i++)
    {
        (*arr)[i] = (char*)malloc(*num_length + 1 * sizeof(char));
        memset((*arr)[i], '\0', *num_length + 1);
    }

    i = -1;
    while ((++i < *num_lines) && fgets((*arr)[i], *num_length + 1, f))
        (*arr)[i][strcspn((*arr)[i], "\n")] = '\0';

    fclose(f);
}

int min(int *arr, int n)
{
    int min = INT_MAX, i;
    for (i = 0; i < n; i++)
        min = arr[i] < min ? arr[i] : min;
    return min;
}
