#!/usr/bin/env bash

part1()
{
    [[ -f "${PWD}/input" ]] && {
        mapfile -t coords < "${PWD}/input"

        for coord in "${coords[@]}"; do
            IFS="," read -r x y <<< "${coord}"
            ((x_max = x > ${x_max:=0} ? x : x_max))
            ((y_max = y > ${y_max:=0} ? y : y_max))
        done

        for ((i = 0; i < x_max + 1; i++)); do
            for ((j = 0; j < y_max + 1; j++)); do
                printf "\\e[2KChecking (%d, %d)\r" "$i" "$j"

                unset dists
                count="0"

                ((min_dists = 2 ** 32))

                for id in "${!coords[@]}"; do
                    IFS="," read -r x y <<< "${coords[${id}]}"
                    ((x_dist = i - x, y_dist = j - y))
                    ((dists[${id}] = ${x_dist//'-'} + ${y_dist//'-'}))
                done

                for k in "${dists[@]}"; do
                    ((min_dists = k < min_dists ? k : min_dists))
                done

                for k in "${!dists[@]}"; do
                    ((min_dists == dists[k])) && \
                        ((count++, id = k))
                done

                ((count == 1)) && {
                    : "${size[$id]:=0}"
                    ((size[id]++))

                    [[ "${infinite[$id]}" != true ]] && \
                    ((i == 0 || i == max_x || j == 0 || j == max_y)) && \
                        infinite[$id]="true"
                }
            done
        done

        for id in "${!size[@]}"; do
            [[ "${infinite[$id]}" != true ]] && \
                ((max_area = size[id] > max_area ? size[id] : max_area))
        done

        printf "\\e[2KMaximum area: %d\\n" "${max_area}"
    }
}

part2()
{
    area="0"
    for ((i = 0; i < x_max + 1; i++)); do
        for ((j = 0; j < y_max + 1; j++)); do
            printf "\\e[2KChecking (%d, %d)\r" "$i" "$j"
            unset total_dist
            total_dist="0"
            for id in "${!coords[@]}"; do
                IFS="," read -r x y <<< "${coords[${id}]}"
                ((x_dist = i - x, y_dist = j - y))
                ((total_dist += (${x_dist//'-'} + ${y_dist//'-'})))
            done
            ((area += total_dist < 10000 ? 1 : 0))
        done
    done

    printf "\\e[2KArea of safe region: %d\\n" "${area}"
}

main()
{
    part1
    part2
}

main
