#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FALSE 0
#define TRUE !FALSE
#define FORMAT "#%d @ %d,%d: %dx%d"
#define MAX 128

typedef struct
{
    int id, x, y, length, width;
} claim;

int part1(claim **claims, int ***fabric, int* fabric_length);
void part2(claim **claims, int ***fabric, int n);
void init_string_array(char ***arr, int n, int m);
void init_claims(claim **claims, int n);
void init_fabric(int ***fabric, int x, int y);

int main(void)
{
    claim *claims = NULL;
    int **fabric = NULL;
    int num_lines = 0, fabric_length, i;

    if ((num_lines = part1(&claims, &fabric, &fabric_length)) != -1)
        part2(&claims, &fabric, num_lines);

    if (claims)
    {
        free(claims);
        claims = NULL;
    }

    if (fabric)
    {
        for (i = 0; i < fabric_length; i++)
        {
            free(fabric[i]);
            fabric[i] = NULL;
        }
        free(fabric);
        fabric = NULL;
    }

    return 0;
}

int part1(claim **claims, int ***fabric, int* fabric_length)
{
    FILE *f = fopen("./input", "r");
    char **strs = NULL;
    int num_lines = 0, ch, i, j, k;
    int id, x, y, length, width;
    int x_max = 0, y_max = 0, temp = 0;
    int overlap = 0;

    if (ferror(f))
        return -1;

    while ((ch = fgetc(f)) != EOF && ! ferror(f))
        num_lines += ch == '\n' ? 1 : 0;
    fseek(f, 0, SEEK_SET);

    init_claims(claims, num_lines);
    init_string_array(&strs, num_lines, MAX);

    strs = (char**)malloc(num_lines * sizeof(char*));
    for (i = 0; i < num_lines; i++)
    {
        strs[i] = (char*)malloc(MAX * sizeof(char));
        memset(strs[i], '\0', MAX);
    }

    i = 0;
    while ((i < num_lines) && fgets(strs[i++], MAX, f));

    for (i = 0; i < num_lines; i++)
    {
        strs[i][strcspn(strs[i], "\n")] = '\0';
        sscanf(strs[i], FORMAT, &id, &x, &y, &length, &width);
        (*claims)[i].id = id;
        (*claims)[i].x = x;
        (*claims)[i].y = y;
        (*claims)[i].length = length;
        (*claims)[i].width = width;

        temp = x + length;
        x_max = temp > x_max ? temp : x_max;

        temp = y + width;
        y_max = temp > y_max ? temp : y_max;

        free(strs[i]);
        strs[i] = NULL;
    }

    free(strs);
    strs = NULL;

    (*fabric_length) = x_max;
    init_fabric(fabric, x_max, y_max);

    for (i = 0; i < num_lines; i++)
        for (j = (*claims)[i].x; j < (*claims)[i].x + (*claims)[i].length; j++)
            for (k = (*claims)[i].y; k < (*claims)[i].y + (*claims)[i].width; k++)
            {
                (*fabric)[j][k]++;
                overlap += (*fabric)[j][k] == 2 ? 1 : 0;
            }

    if (f)
        fclose(f);

    printf("Area overlapped: %d\n", overlap);
    return num_lines;
}

void part2(claim **claims, int ***fabric, int n)
{
    int i, j, k;
    int invalid = FALSE;

    for (i = 0; i < n; i++)
    {
        invalid = FALSE;
        j = (*claims)[i].x;
        while (j < (*claims)[i].x + (*claims)[i].length && ! invalid)
        {
            k = (*claims)[i].y;
            while (k < (*claims)[i].y + (*claims)[i].width && ! invalid)
            {
                invalid = (*fabric)[j][k] != 1 ? TRUE : FALSE;
                k++;
            }
            j++;
        }

        if (! invalid)
        {
            printf("Non-overlapping id: %d\n", (*claims)[i].id);
            break;
        }
    }
}

void init_string_array(char ***arr, int n, int m)
{
    int i;
    (*arr) = (char**)malloc(n * sizeof(char*));
    for (i = 0; i < n; i++)
    {
        (*arr)[i] = (char*)malloc(m * sizeof(char));
        memset((*arr)[i], '\0', MAX);
    }

}

void init_claims(claim **claims, int n)
{
    int i;
    (*claims) = (claim*)malloc(n * sizeof(claim));
    for (i = 0; i < n; i++)
    {
        (*claims)[i].id = 0;
        (*claims)[i].x = 0;
        (*claims)[i].y = 0;
        (*claims)[i].length = 0;
        (*claims)[i].width = 0;
    }
}

void init_fabric(int ***fabric, int x, int y)
{
    int i, j;
    (*fabric) = (int**)malloc(x * sizeof(int*));
    for (i = 0; i < y; i++)
    {
        (*fabric)[i] = (int*)malloc(y * sizeof(int));
        for (j = 0; j < y; j++)
            (*fabric)[i][j] = 0;
    }
}
