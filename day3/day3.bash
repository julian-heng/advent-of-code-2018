#!/usr/bin/env bash

part1()
{
    [[ -f "${PWD}/input" ]] && {
        mapfile -t file < "${PWD}/input"
        i=0

        for line in "${file[@]}"; do
            read -r id _ coords size <<< "${line}"
            id="${id/'#'}"
            coords="${coords/':'}"
            IFS="," read -r x y <<< "${coords}"
            IFS="x" read -r length width <<< "${size}"

            printf -v claims[$i] "%s:" "$x" "$y" "${length}"
            claims[$i]+="${width}"
            ((i++))
        done

        for id in "${claims[@]}"; do
            IFS=":" read -r x y length width <<< "${id}"
            for ((i = x; i < x + length; i++)); do
                for ((j = y; j < y + width; j++)); do
                    : "${fabric[$i, $j]:=0}"
                    ((++fabric[$i, $j] == 2 && overlap++))
                done
            done
        done
    }

    printf "Area overlapped: %d\n" "${overlap}"
}

part2()
{
    for id in "${!claims[@]}"; do
        IFS=":" read -r x y length width <<< "${claims[$id]}"
        unset invalid
        i="$x"
        while ((i < x + length)) && [[ ! "${invalid}" ]]; do
            j="$y"
            while ((j < y + width)) && [[ ! "${invalid}" ]]; do
                ((fabric[$i, $j] != 1)) && \
                    invalid="true"
                ((j++))
            done
            ((i++))
        done

        [[ ! "${invalid}" ]] && {
            printf "Non-overlapping id: %s\n" "$((id + 1))"
            break
        }
    done
}

main()
{
    declare -A fabric
    part1
    part2
}

main
