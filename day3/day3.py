#!/usr/bin/env python3

from itertools import product

class claim:
    id = 0
    x = 0
    y = 0
    length = 0
    width = 0
    def __init__(self, in_id, in_x, in_y, in_length, in_width):
        self.id = int(in_id)
        self.x = int(in_x)
        self.y = int(in_y)
        self.length = int(in_length)
        self.width = int(in_width)

def part1(lines):
    claims = dict()
    fabric = [[]]
    overlap = 0

    for line in lines:
        split = line.split()
        id = split[0][1:]
        x, y = split[2][:-1].split(",")
        length, width = split[3].split("x")
        claims[id] = claim(id, x, y, length, width)

    max_x = max([i.x + i.length for i in claims.values()])
    max_y = max([i.y + i.width for i in claims.values()])
    fabric = [[0] * max_x for j in range(max_y)]

    for id in claims.values():
        a = range(id.x, id.x + id.length)
        b = range(id.y, id.y + id.width)
        for i, j in product(a, b):
            fabric[i][j] += 1
            overlap += 1 if fabric[i][j] == 2 else 0

    print("Area overlapped: {}".format(overlap))
    return claims, fabric

def part2(claims, fabric):
    for id in claims.values():
        invalid = False
        i = id.x
        while i < id.x + id.length and not invalid:
            j = id.y
            while j < id.y + id.width and not invalid:
                invalid = True if fabric[i][j] != 1 else False
                j += 1
            i += 1

        if not invalid:
            print("Non-overlapping id: {}".format(id.id))
            break

def main():
    with open("input") as f:
        file = f.readlines()

    claims, fabric = part1(file)
    part2(claims, fabric)

main()
