# [Advent of Code 2018](https://adventofcode.com/2018)

This repository contains my solutions to Advent of Code 2018, written in
`pure bash`, `python3` and `C89`.

## Restrictions / Rules
 - bash
    - No external programs
 - python3
    - None
 - C89
    - No valgrind errors or memory leaks
