#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "../resources/uthash/include/uthash.h"

#define FALSE 0
#define TRUE !FALSE
#define MAX 128

typedef struct
{
    int id;
    int minutes[60], sum, max;
    UT_hash_handle hh;
} guard;

void prep(guard **guards, char **file, int n);
void part1(guard **guards);
void part2(guard **guards);

void read_file(char ***arr, int *num_lines);
int last_int(char *str);
void swap_str(char **a, char **b);
void quick_sort(char **arr, int n);
int sort_sum(guard *a, guard *b);
int sort_max(guard *a, guard *b);

int main(void)
{
    char **file = NULL;
    int file_length = 0, i;

    guard *guards = NULL;
    guard *entry = NULL;
    guard *result = NULL;

    read_file(&file, &file_length);

    if (file)
    {
        prep(&guards, file, file_length);
        part1(&guards);
        part2(&guards);

        HASH_ITER(hh, guards, entry, result)
        {
            HASH_DEL(guards, entry);
            free(entry);
            entry = NULL;
        }

        for (i = 0; i < file_length; i++)
        {
            free(file[i]);
            file[i] = NULL;
        }

        free(file);
        file = NULL;
    }

    return 0;
}

void prep(guard **guards, char **file, int n)
{
    int i, j, guard_id, start, stop, sum, max;

    guard *entry = NULL;
    guard *result = NULL;

    guard_id = 0;
    start = 0;
    stop = 0;
    (*guards) = NULL;

    quick_sort(file, n);

    for (i = 0; i < n; i++)
        if (strstr(file[i], "Guard"))
            guard_id = last_int(file[i]);
        else if (strstr(file[i], "asleep"))
            start = last_int(file[i]);
        else if (strstr(file[i], "wakes"))
        {
            stop = last_int(file[i]);

            result = NULL;
            HASH_FIND_INT((*guards), &guard_id, result);

            if (! result)
            {
                entry = (guard*)malloc(sizeof(guard));
                entry -> id = guard_id;
                for (j = 0; j < 60; j++)
                    entry -> minutes[j] = 0;
                entry -> sum = 0;
                entry -> max = 0;

                HASH_ADD_INT((*guards), id, entry);
                HASH_FIND_INT((*guards), &guard_id, result);
            }

            for (j = start; j < stop; j++)
                (result -> minutes[j])++;
        }

    HASH_ITER(hh, (*guards), entry, result)
    {
        sum = 0;
        max = 0;

        for (i = 0; i < 60; i++)
        {
            sum += entry -> minutes[i];
            max = entry -> minutes[i] < max ? max : entry -> minutes[i];
        }

        entry -> sum = sum;
        entry -> max = max;
    }
}

void part1(guard **guards)
{
    int i, max;
    guard *entry = NULL;
    guard *result = NULL;

    HASH_SRT(hh, (*guards), sort_sum);
    HASH_ITER(hh, (*guards), entry, result)
    {
        break;
    }

    i = -1;
    while ((i++ < 60) && (max = entry -> minutes[i]) != entry -> max);
    printf("ID: %d\n", entry -> id);
    printf("Minute: %d\n", i);
    printf("Part1: %d\n\n", entry -> id * i);
}

void part2(guard **guards)
{
    int i, max;
    guard *entry = NULL;
    guard *result = NULL;

    HASH_SRT(hh, (*guards), sort_max);
    HASH_ITER(hh, (*guards), entry, result)
    {
        break;
    }

    i = -1;
    while ((i++ < 60) && (max = entry -> minutes[i]) != entry -> max);
    printf("ID: %d\n", entry -> id);
    printf("Minute: %d\n", i);
    printf("Part2: %d\n", entry -> id * i);
}

void read_file(char ***arr, int *num_lines)
{
    FILE *f = fopen("./input", "r");
    int ch, i;

    if (ferror(f))
        return;

    *num_lines = 0;
    while ((ch = fgetc(f)) != EOF && ! ferror(f))
        *num_lines += ch == '\n' ? 1 : 0;
    fseek(f, 0, SEEK_SET);

    (*arr) = (char**)malloc(*num_lines * sizeof(char*));
    for (i = 0; i < *num_lines; i++)
    {
        (*arr)[i] = (char*)malloc(MAX * sizeof(char));
        memset((*arr)[i], '\0', MAX);
    }

    i = -1;
    while ((++i < *num_lines) && fgets((*arr)[i], MAX, f))
        (*arr)[i][strcspn((*arr)[i], "\n")] = '\0';

    fclose(f);
}

int last_int(char *str)
{
    int value = -1;
    char* ch;

    if (str && *str)
    {
        ch = str + strlen(str) - 1;
        while (--ch > str && ! isdigit(*(ch - 1)));
        while (--ch > str && isdigit(*(ch - 1)));
        sscanf(ch, "%d", &value);
    }

    return value;
}

void swap_str(char **a, char **b)
{
    char *tmp = *a;
    *a = *b;
    *b = tmp;
}

void quick_sort(char **arr, int n)
{
    int i, pivot = 0;

    if (n < 2)
        return;

    swap_str(arr + (n / 2), arr + n - 1);
    for (i = 0; i < n - 1; i++)
        if (strcmp(arr[i], arr[n - 1]) < 0)
            swap_str(arr + i, arr + pivot++);
    swap_str(arr + pivot, arr + n - 1);

    quick_sort(arr, pivot++);
    quick_sort(arr + pivot, n - pivot);
}

int sort_sum(guard *a, guard *b)
{
    if (a -> sum == b -> sum)
        return 0;
    return (a -> sum > b -> sum) ? -1 : 1;
}

int sort_max(guard *a, guard *b)
{
    if (a -> max == b -> max)
        return 0;
    return (a -> max > b -> max) ? -1 : 1;
}
