#!/usr/bin/env python3

from itertools import combinations
import re

def part1(lines):
    guards = dict()
    start, stop, guard = 0, 0, 0
    for line in lines:
        values = re.findall("\d+", line)
        if "Guard" in line:
            guard = int(values[-1])
        elif "asleep" in line:
            start = int(values[-1])
        elif "wakes" in line:
            stop = int(values[-1])
            guards.setdefault(guard, [0] * 60)
            for i in range(start, stop):
                guards[guard][i] += 1

    id = max(guards.keys(), key=(lambda k: sum(guards[k])))
    min = guards[id].index(max(guards[id]))

    print("ID: {}".format(id))
    print("Minute: {}".format(min))
    print("Part1: {}\n".format(id * min))

    return guards

def part2(guards):
    id = max(guards.keys(), key=(lambda k: max(guards[k])))
    min = guards[id].index(max(guards[id]))

    print("ID: {}".format(id))
    print("Minute: {}".format(min))
    print("Part2: {}".format(id * min))

def main():
    with open("input") as f:
        file = f.read().splitlines()

    file.sort()
    part2(part1(file))

main()
