#!/usr/bin/env bash

_qsort()
{
    (($# == 0)) && {
        unset qreturn
        return
    }

    local pivot="$1"; shift
    local i
    local -a higher
    local -a lower

    for i in "$@"; do
        if [[ "$i" < "${pivot}" ]]; then
            lower+=("$i")
        else
            higher+=("$i")
        fi
    done

    _qsort "${higher[@]}"
    higher=("${qreturn[@]}")

    _qsort "${lower[@]}"
    qreturn+=("${pivot}" "${higher[@]}")
}

qsort()
(
    qreturn=()
    _qsort "$@"
    printf "%s\\n" "${qreturn[@]}"
)

part1()
{
    [[ -f "${PWD}/input" ]] && {
        mapfile -t file < "${PWD}/input"
        mapfile -t file < <(qsort "${file[@]}")

        for line in "${file[@]}"; do
            case "${line}" in
                *"Guard"*)
                    read -r _ _ _ id _ <<< "${line}"
                    id="${id/'#'}"

                    if [[ "${midnight_list[$id]}" ]]; then
                        read -ra midnight <<< "${midnight_list[$id]}"
                    else
                        for j in {0..59}; do
                            midnight[$j]="0"
                        done
                    fi
                ;;

                *"asleep"*)
                    read -r _ start _ <<< "${line}"
                    start="${start/*:}"
                    start="${start/']'}"
                    start="$((10#${start}))"
                ;;

                *"wakes"*)
                    read -r _ stop _ <<< "${line}"
                    stop="${stop/*:}"
                    stop="${stop/']'}"
                    stop="$((10#${stop}))"

                    for ((i = start; i < stop; i++)); do
                        ((midnight[i]++))
                    done

                    printf -v midnight_list[$id] "%d " "${midnight[@]}"
                ;;
            esac
        done

        for id in "${!midnight_list[@]}"; do
            unset total_time
            unset max

            read -ra midnight <<< "${midnight_list[$id]}"
            for i in "${midnight[@]}"; do
                ((total_time += i, max = i > ${max:=0} ? i : ${max:=0}))
            done

            printf -v midnight_list[$id] "%d " \
                "${total_time}" \
                "${max}" \
                "${midnight[@]}"
        done

        unset max
        for id in "${!midnight_list[@]}"; do
            read -ra midnight <<< "${midnight_list[$id]}"
            ((max = midnight[0] > ${max:=0} ? midnight[0] : ${max:=0}))
        done

        for id in "${!midnight_list[@]}"; do
            max_min="0"
            read -ra midnight <<< "${midnight_list[$id]}"
            ((max == midnight[0])) && \
                for minute in "${midnight[@]:2}"; do
                    if ((midnight[1] != minute)); then
                        ((max_min++))
                    else
                        break 2
                    fi
                done
        done

        printf "ID: %d\\nMinute: %d\\n" "${id}" "${max_min}"
        printf "Part1: %d\\n\\n" "$((id * max_min))"
    }
}

part2()
{
    [[ "${midnight_list[*]}" ]] && {
        unset max
        for id in "${!midnight_list[@]}"; do
            read -ra midnight <<< "${midnight_list[$id]}"
            ((max = midnight[1] > ${max:=0} ? midnight[1] : ${max:=0}))
        done

        for id in "${!midnight_list[@]}"; do
            max_min="0"
            read -ra midnight <<< "${midnight_list[$id]}"
            ((max == midnight[1])) && \
                for minute in "${midnight[@]:2}"; do
                    if ((midnight[1] != minute)); then
                        ((max_min++))
                    else
                        break 2
                    fi
                done
        done

        printf "ID: %d\\nMinute: %d\\n" "${id}" "${max_min}"
        printf "Part2: %d\\n" "$((id * max_min))"
    }
}

main()
{
    part1
    part2
}

main
