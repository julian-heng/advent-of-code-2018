all:
	bash -c 'for i in ./day*; do ( cd "$$i" && make ); done'

clean:
	bash -c 'for i in ./day*; do ( cd "$$i" && make clean); done'
