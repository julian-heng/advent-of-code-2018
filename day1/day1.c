#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../resources/uthash/include/uthash.h"

#define FALSE 0
#define TRUE !FALSE

typedef struct
{
    int freq;
    UT_hash_handle hh;
} hash_entry;

void read_file(int **arr, int *num_lines);
void part1(int *arr, int length);
void part2(int *arr, int length);

int main(void)
{
    int *arr = NULL;
    int lines = 0;

    read_file(&arr, &lines);
    part1(arr, lines);
    part2(arr, lines);

    free(arr);
    arr = NULL;

    return 0;
}

void read_file(int **arr, int *num_lines)
{
    FILE *f = fopen("./input", "r");
    int ch, i;

    if (ferror(f))
        return;

    *num_lines = 0;
    while ((ch = fgetc(f)) != EOF && ! ferror(f))
        if (ch == '\n') *num_lines += 1;
    fseek(f, 0, SEEK_SET);

    if (*num_lines == 0) (*num_lines)++;

    (*arr) = (int*)malloc((*num_lines) * sizeof(int));
    memset(*arr, 0, *num_lines);

    i = -1;
    while ((++i < *num_lines) && fscanf(f, "%d", (*arr) + i) == 1);
    fclose(f);
}

void part1(int *arr, int length)
{
    int freq = 0, i;
    for (i = 0; i < length; i++)
        freq += arr[i];
    printf("Resulting frequency: %d\n", freq);
}

void part2(int *arr, int length)
{
    int freq = 0, i, match = FALSE;

    hash_entry *table = NULL;
    hash_entry *entry = NULL;
    hash_entry *result = NULL;

    while (! match)
    {
        i = -1;
        while (++i < length && ! match)
        {
            freq += arr[i];

            HASH_FIND_INT(table, &freq, result);
            if (result)
                match = TRUE;
            else
            {
                entry = (hash_entry*)malloc(sizeof(hash_entry));
                entry -> freq = freq;
                HASH_ADD_INT(table, freq, entry);
            }
        }
    }

    HASH_ITER(hh, table, entry, result)
    {
        HASH_DEL(table, entry);
        free(entry);
    }

    printf("First Duplication: %d\n", freq);
}
