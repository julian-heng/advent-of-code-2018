#!/usr/bin/env python3

def part1(lines):
    print("Resulting frequency: {}".format(sum([int(i) for i in lines])))

def part2(lines):
    def check(i, d):
        if i in d:
            return True
        else:
            d[i] = 1
            return False

    freq_cache = [ int(line) for line in lines ]
    freq, iter = 0, 0
    neg, pos = dict(), dict()
    match = False

    while not match:
        print("Iteration: {}\r".format(iter), end="")
        iter += 1

        for _freq in freq_cache:
            freq += _freq
            match = check(freq, neg if freq < 0 else pos)
            if match:
                break

    print("First Duplication: {}".format(freq))

def main():
    with open("input") as f:
        file = f.readlines()

    part1(file)
    part2(file)

main()
