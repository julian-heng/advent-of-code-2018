#!/usr/bin/env bash

part1()
{
    [[ -f "${PWD}/input" ]] && {
        freq="0"
        while IFS="" read -r line; do
            ((freq = freq ${line}))
        done < "${PWD}/input"
        printf "Resulting frequency: %d\\n" "${freq}"
    }
}

part2()
{
    [[ -f "${PWD}/input" ]] && {
        freq="0"
        mapfile -t file < "${PWD}/input"

        while :; do
            printf "Iteration: %d\\r" "${count:=0}"
            ((count++))
            for line in "${file[@]}"; do
                : "$((freq = freq ${line}))"

                if (((_freq = -freq) < 0)); then
                    if [[ ! "${pos_table[${freq}]}" ]]; then
                        pos_table[${freq}]="1"
                    else
                        printf "First Duplicate: %d\\n" "${freq}"
                        return
                    fi
                else
                    if [[ ! "${neg_table[${_freq}]}" ]]; then
                        neg_table[${_freq}]="1"
                    else
                        printf "First Duplicate: %d\\n" "${freq}"
                        return
                    fi
                fi
            done
        done
    }
}

main()
{
    part1
    part2
}

main
