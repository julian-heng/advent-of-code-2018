#!/usr/bin/env bash

part1()
{
    [[ -f "${PWD}/input" ]] && {
        mapfile -t file < "${PWD}/input"

        two_dupe="0"
        three_dupe="0"

        for line in "${file[@]}"; do
            for i in {0..25}; do
                hash_table[$i]="0"
            done
            unset inv_table

            for ((i = 0; i < "${#line}"; i++)); do
                printf -v index '%d' "'${line:$i:1}"
                ((hash_table[$((index - 97))]++))
            done

            for entry in "${hash_table[@]}"; do
                [[ ! "${inv_table[$entry]}" ]] && \
                    inv_table[${entry}]="1"
            done

            [[ "${inv_table[2]}" ]] && \
                ((two_dupe++))
            [[ "${inv_table[3]}" ]] && \
                ((three_dupe++))
        done

        printf "Hash: %d\\n" "$((two_dupe * three_dupe))"
    }
}

part2()
{
    [[ -f "${PWD}/input" ]] && {
        mapfile -t file < "${PWD}/input"

        for ((i = 0; i < ${#file[@]}; i++)); do
            for ((j = i + 1; j < ${#file[@]}; j++)); do
                unset str_3
                str_1="${file[$i]}"
                str_2="${file[$j]}"
                len="${#str_1}"

                for ((k = 0; k < ${len}; k++)); do
                    [[ "${str_1:$k:1}" == "${str_2:$k:1}" ]] && \
                        str_3+="${str_1:$k:1}"
                done

                ((len - ${#str_3} <= 1)) && \
                    printf "%s\\n" "${str_3}"
            done
        done
    }
}

main()
{
    part1
    part2
}

main
