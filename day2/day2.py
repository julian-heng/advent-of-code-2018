#!/usr/bin/env python3

from itertools import combinations

def part1(lines):
    table_arr = [{line.count(letter): letter for letter in set(line)} for line in lines]
    two = sum([2 in table for table in table_arr])
    three = sum([3 in table for table in table_arr])
    print("Hash: {}".format(two * three))

def part2(lines):
    for i, j in combinations(lines, 2):
        length = len(i)
        str = "".join(a for (a, b) in zip(i, j) if a == b)

        if length - len(str) <= 1:
            print(str)

def main():
    with open("input") as f:
        file = f.read().splitlines()

    part1(file)
    part2(file)

main()
