#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../resources/uthash/include/uthash.h"

#define CHECK(a, b, c, d) \
    if ((a) == (b) && ! *(d)) \
    { \
        (*(c))++; \
        *(d) = TRUE; \
    }

#define FALSE 0
#define TRUE !FALSE
#define MAX 128

typedef struct
{
    int key, value;
    UT_hash_handle hh;
} hash_entry;

void part1(void);
void part2(void);
void check();

int main(void)
{
    part1();
    part2();
    return 0;
}

void part1(void)
{
    FILE *f = fopen("./input", "r");
    char str[MAX];
    int i, letter;

    int two = 0, three = 0;
    int _two = FALSE, _three = FALSE;

    hash_entry *table = NULL;
    hash_entry *entry = NULL;
    hash_entry *result = NULL;

    if (! ferror(f))
        while (fgets(str, MAX, f))
        {
            table = NULL;
            _two = FALSE;
            _three = FALSE;
            str[strcspn(str, "\n")] = '\0';

            for (i = 0; i < strlen(str); i++)
            {
                letter = str[i];
                HASH_FIND_INT(table, &letter, result);

                if (! result)
                {
                    entry = (hash_entry*)malloc(sizeof(hash_entry));
                    entry -> key = letter;
                    entry -> value = 0;
                    HASH_ADD_INT(table, key, entry);
                    HASH_FIND_INT(table, &letter, result);
                }

                (result -> value)++;
            }

            HASH_ITER(hh, table, entry, result)
            {
                CHECK(entry -> value, 2, &two, &_two);
                CHECK(entry -> value, 3, &three, &_three);
            }

            HASH_ITER(hh, table, entry, result)
            {
                HASH_DEL(table, entry);
                free(entry);
            }
        }

    if (f)
        fclose(f);

    printf("Hash: %d\n", two * three);
}

void part2(void)
{
    FILE *f = fopen("./input", "r");
    char **arr;
    char *out;
    int num_lines = 0, ch, i, j, k, count;

    if (ferror(f))
        return;

    while ((ch = fgetc(f)) != EOF && ! ferror(f))
        num_lines += ch == '\n' ? 1 : 0;

    fseek(f, 0, SEEK_SET);

    arr = (char**)malloc(num_lines * sizeof(char*));
    for (i = 0; i < num_lines; i++)
    {
        arr[i] = (char*)malloc(MAX * sizeof(char));
        memset(arr[i], '\0', MAX);
    }

    i = 0;
    while ((i < num_lines) && fgets(arr[i++], MAX, f));

    for (i = 0; i < num_lines; i++)
        arr[i][strcspn(arr[i], "\n")] = '\0';

    for (i = 0; i < num_lines; i++)
        for (j = i + 1; j < num_lines; j++)
        {
            count = 0;
            out = (char*)malloc((strlen(arr[i]) + 1) * sizeof(char));
            memset(out, '\0', strlen(arr[i]) + 1);
            for (k = 0; k < strlen(arr[i]); k++)
                if (arr[i][k] == arr[j][k])
                    out[count++] = arr[i][k];

            if (strlen(arr[i]) - strlen(out) <= 1)
                printf("%s\n", out);

            free(out);
            out = NULL;
        }

    for (i = 0; i < num_lines; i++)
    {
        free(arr[i]);
        arr[i] = NULL;
    }

    free(arr);
    arr = NULL;

    if (f)
        fclose(f);
}
