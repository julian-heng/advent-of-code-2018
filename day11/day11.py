#!/usr/bin/env python3

import sys

def get_power(x, y, serial):
    id = x + 10
    pow = id * y + serial
    pow = (pow * id) // 100 % 10 - 5
    return pow

def prep(serial):
    s = [[0] * 301 for _ in range(301)]

    for x in range(1, 301):
        for y in range(1, 301):
            s[y][x] = get_power(x, y, serial)
            s[y][x] += s[y - 1][x] + s[y][x - 1] - s[y - 1][x - 1]

    return s

def part1(sums):
    best = -sys.maxsize
    for x in range(3, 301):
        for y in range(3, 301):
            tmp = sums[y][x] - sums[y - 3][x] - sums[y][x - 3] + sums[y - 3][x - 3]
            if tmp > best:
                best = tmp
                bx = x - 2
                by = y - 2
    print("Part 1: {},{}".format(bx, by))

def part2(sums):
    best = -sys.maxsize
    for s in range(1, 301):
        for x in range(s, 301):
            for y in range(s, 301):
                tmp = sums[y][x] - sums[y - s][x] - sums[y][x - s] + sums[y - s][x - s]
                if tmp > best:
                    best = tmp
                    bx = x - s + 1
                    by = y - s + 1
                    bs = s
    print("Part 2: {},{},{}".format(bx, by, bs))

def main():
    with open("input") as f:
        serial = int(f.readline().strip())
    sums = prep(serial)
    part1(sums)
    part2(sums)

main()
