#!/usr/bin/env bash

get_power()
{
    local x="$1"
    local y="$2"
    local serial="$3"

    ((id = x + 10))
    ((pow = id * y + serial))
    ((pow = (pow * id) / 100 % 10 - 5))
    printf "%d" "${pow}"
}

part1()
{
    [[ -f "${PWD}/input" ]] && {
        serial="$(< "${PWD}/input")"

        for x in {1..301}; do
            for y in {1..301}; do
                printf "\\e[2K[Sum Table] Calculating %d, %d\\r" "$x" "$y"
                sums["$x,$y"]="$(get_power "$x" "$y" "${serial}")"
                ((sums["$x,$y"] += sums["$((x - 1)),$y"] + sums["$x,$((y - 1))"] - sums["$((x - 1)),$((y - 1))"]))
            done
        done

        best="-1000000"

        for ((x = 3; x < 301; x++)); do
            for ((y = 3; y < 301; y++)); do
                printf "\\e[2K[3x3] Calculating %d, %d\\r" "$x" "$y"
                ((tmp = sums["$x,$y"] - sums["$x,$((y - 3))"] - sums["$((x - 3)),$y"] + sums["$((x - 3)),$((y - 3))"]))
                ((tmp > best)) && {
                    best="${tmp}"
                    bx="$((x - 2))"
                    by="$((y - 2))"
                }
            done
        done

        printf "\\e[2KPart 1: %d,%d\\n" "${bx}" "${by}"
    }
}

part2()
{
    best="-1000000"
    for ((s = 1; s < 301; s++)); do
        for ((x = s; x < 301; x++)); do
            for ((y = s; y < 301; y++)); do
                printf "\\e[2K[%dx%d] Calculating %d, %d\\r" "$s" "$s" "$x" "$y"
                ((tmp = sums["$x,$y"] - sums["$x,$((y - s))"] - sums["$((x - s)),$y"] + sums["$((x - s)),$((y - s))"]))
                ((tmp > best)) && {
                    best="${tmp}"
                    bx="$((x - s + 1))"
                    by="$((y - s + 1))"
                    bs="$s"
                }
            done
        done
    done

    printf "\\e[2KPart 2: %d,%d,%d\\n" "${bx}" "${by}" "${bs}"
}

main()
{
    declare -A sums
    part1
    part2
}

main
