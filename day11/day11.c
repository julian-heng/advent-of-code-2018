#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define FALSE 0
#define TRUE !FALSE
#define MAX 301

#define POW(a, b, c) \
    ((((a) + 10) * (b) + (c)) * ((a) + 10)) / 100 % 10 - 5

int read_int();
void prep(int s[MAX][MAX], int serial);
int get_area(int s[MAX][MAX], int x, int y, int sz);
void part1(int sums[MAX][MAX]);
void part2(int sums[MAX][MAX]);

int main(void)
{
    int serial = read_int();
    int sums[MAX][MAX] = {{0}};

    prep(sums, serial);
    part1(sums);
    part2(sums);
    return 0;
}

int read_int()
{
    FILE *f = fopen("input", "r");
    int a = 0;

    if (f)
    {
        fscanf(f, "%d", &a);
        fclose(f);
    }

    return a;
}

void prep(int s[MAX][MAX], int serial)
{
    int x, y;
    for (x = 1; x < MAX; x++)
        for (y = 1; y < MAX; y++)
        {
            s[y][x] = POW(x, y, serial);
            s[y][x] += s[y - 1][x] + s[y][x - 1] - s[y - 1][x - 1];
        }
}

int get_area(int s[MAX][MAX], int x, int y, int sz)
{
    return s[y][x] - s[y - sz][x] - s[y][x - sz] + s[y - sz][x - sz];
}

void part1(int sums[MAX][MAX])
{
    int best = INT_MIN;
    int x, y, bx, by, tmp;

    for (x = 3; x < MAX; x++)
        for (y = 3; y < MAX; y++)
        {
            tmp = get_area(sums, x, y, 3);
            if (tmp > best)
            {
                best = tmp;
                bx = x - 2;
                by = y - 2;
            }
        }
    printf("Part 1: %d,%d\n", bx, by);
}

void part2(int sums[MAX][MAX])
{
    int best = INT_MIN;
    int x, y, s, bx, by, bs, tmp;

    for (s = 1; s < MAX; s++)
        for (x = s; x < MAX; x++)
            for (y = s; y < MAX; y++)
            {
                tmp = get_area(sums, x, y, s);
                if (tmp > best)
                {
                    best = tmp;
                    bx = x - s + 1;
                    by = y - s + 1;
                    bs = s;
                }
            }
    printf("Part 2: %d,%d,%d\n", bx, by, bs);
}
