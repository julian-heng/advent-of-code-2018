#!/usr/bin/env bash

part1()
{
    [[ -f "${PWD}/input" ]] && {
        orig_polymer="$(< "${PWD}/input")"

        polymer="${orig_polymer}"
        reacted="${polymer}"
        for i in {a..z}; do
            reacted="${reacted//$i${i^^}}"
            reacted="${reacted//${i^^}$i}"
        done

        while [[ "${reacted}" != "${polymer}" ]]; do
            polymer="${reacted}"
            for i in {a..z}; do
                reacted="${reacted//$i${i^^}}"
                reacted="${reacted//${i^^}$i}"
            done
        done
        printf "No. of Units: %d\\n" "${#reacted}"
    }
}

part2()
{
    for i in {a..z}; do
        polymer="${orig_polymer}"
        reacted="${polymer}"
        reacted="${reacted//$i}"
        reacted="${reacted//${i^^}}"

        while [[ "${reacted}" != "${polymer}" ]]; do
            polymer="${reacted}"
            for j in {a..z}; do
                reacted="${reacted//$j${j^^}}"
                reacted="${reacted//${j^^}$j}"
            done
        done

        ((min = ${min:=${#reacted}} > ${#reacted} ? ${#reacted} : min))
    done

    printf "Shortest polymer: %d\\n" "${min}"
}

main()
{
    part1
    part2
}

main
