#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define FALSE 0
#define TRUE !FALSE

#define IGNORE '-'

void part1(char *str);
void part2(char *str);
int react(char *str);
void read_file(char ***arr, int *num_lines, int *num_length);

int main(void)
{
    char **file = NULL, *str;
    int file_lines = 0, file_length = 0, i;

    read_file(&file, &file_lines, &file_length);

    if (file)
    {
        str = (char*)malloc((file_length + 1) * sizeof(char));
        memset(str, '\0', file_length + 1);
        strncpy(str, file[0], file_length + 1);

        part1(str);
        part2(file[0]);

        free(str);
        str = NULL;

        for (i = 0; i < file_lines; i++)
        {
            free(file[i]);
            file[i] = NULL;
        }

        free(file);
        file = NULL;
    }

    return 0;
}

void part1(char *str)
{
    printf("No. of Units: %d\n", react(str));
}

void part2(char *str)
{
    char c, *copy;
    int i, j, len = strlen(str), min = INT_MAX, react_len;

    for (c = 'a'; c <= 'z'; c++)
    {
        i = -1;
        j = -1;
        copy = (char*)malloc((len + 1) * sizeof(char));
        memset(copy, '\0', len + 1);
        while (i++ < len && j < len)
            switch (c ^ str[i])
            {
                case 32: case 0: break;
                default: copy[++j] = str[i];
            }

        react_len = react(copy);
        min = react_len < min ? react_len : min;
        free(copy);
        copy = NULL;
    }

    printf("Shortest polymer: %d\n", min);
}

int react(char *str)
{
    int i;
    char *a, *b, *s;

    i = 0;
    a = str;
    b = str;
    s = str;

    while (*b++)
        if ((*a ^ *b) == 32)
        {
            *a = IGNORE;
            *b = IGNORE;

            i--;
            while (*a == IGNORE && a != s) a--;
        }
        else
        {
            while (*(++a) == IGNORE);
            i++;
        }

    return i;
}

void read_file(char ***arr, int *num_lines, int *num_length)
{
    FILE *f = fopen("./input", "r");
    int ch, i, length = 0;

    if (ferror(f))
        return;

    *num_lines = 0;
    while ((ch = fgetc(f)) != EOF && ! ferror(f))
        if (ch == '\n')
        {
            *num_lines += 1;
            *num_length = length > *num_length ? length : *num_length;
            length = 0;
        }
        else
            length++;
    fseek(f, 0, SEEK_SET);

    if (*num_lines == 0) (*num_lines)++;

    (*arr) = (char**)malloc(*num_lines * sizeof(char*));
    for (i = 0; i < *num_lines; i++)
    {
        (*arr)[i] = (char*)malloc(*num_length + 1 * sizeof(char));
        memset((*arr)[i], '\0', *num_length + 1);
    }

    i = -1;
    while ((++i < *num_lines) && fgets((*arr)[i], *num_length + 1, f))
        (*arr)[i][strcspn((*arr)[i], "\n")] = '\0';

    fclose(f);
}
