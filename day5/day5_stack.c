#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>

#include "../resources/uthash/include/utstack.h"

#define FALSE 0
#define TRUE !FALSE

typedef struct character
{
    char c;
    struct character *next;
} character;

void part1(char *str);
void part2(char *str);

void read_file(char ***arr, int *num_lines, int *num_length);

int main(void)
{
    char **file = NULL;
    int file_lines = 0, file_length = 0, i;

    read_file(&file, &file_lines, &file_length);

    if (file)
    {
        part1(file[0]);
        part2(file[0]);

        for (i = 0; i < file_lines; i++)
        {
            free(file[i]);
            file[i] = NULL;
        }

        free(file);
        file = NULL;
    }

    return 0;
}

void part1(char *str)
{
    int i;
    char c, l;
    character *stack = NULL;
    character *entry = NULL;

    for (i = 0; i < strlen(str); i++)
    {
        c = str[i];
        if (STACK_EMPTY(stack))
        {
            entry = (character*)malloc(sizeof(character));
            entry -> c = c;
            STACK_PUSH(stack, entry);
        }
        else
        {
            STACK_POP(stack, entry);
            l = entry -> c;
            if (!((tolower(l) == tolower(c)) &&
                ((isupper(l) == islower(c)) ||
                 (islower(l) == isupper(c)))))
            {
                STACK_PUSH(stack, entry);
                entry = (character*)malloc(sizeof(character));
                entry -> c = c;
                STACK_PUSH(stack, entry);
            }
        }
    }

    STACK_COUNT(stack, entry, i);

    while (!STACK_EMPTY(stack))
    {
        STACK_POP(stack, entry);
        free(entry);
    }

    printf("No. of Units: %d\n", i);
}

void part2(char *str)
{
    int i, min = INT_MAX;
    char c, l, j;
    character *stack = NULL;
    character *entry = NULL;

    for (j = 'a'; j < 'z'; j++)
    {
        printf("Checking %c\r", j);
        for (i = 0; i < strlen(str); i++)
            if (tolower((c = str[i])) != j)
            {
                if (STACK_EMPTY(stack))
                {
                    entry = (character*)malloc(sizeof(character));
                    entry -> c = c;
                    STACK_PUSH(stack, entry);
                }
                else
                {
                    STACK_POP(stack, entry);
                    l = entry -> c;
                    if (!((tolower(l) == tolower(c)) &&
                        ((isupper(l) == islower(c)) ||
                         (islower(l) == isupper(c)))))
                    {
                        STACK_PUSH(stack, entry);
                        entry = (character*)malloc(sizeof(character));
                        entry -> c = c;
                        STACK_PUSH(stack, entry);
                    }
                }
            }

        STACK_COUNT(stack, entry, i);
        min = i < min ? i : min;

        while (!STACK_EMPTY(stack))
        {
            STACK_POP(stack, entry);
            free(entry);
        }
    }

    printf("Shortest polymer: %d\n", min);
}

void read_file(char ***arr, int *num_lines, int *num_length)
{
    FILE *f = fopen("./input", "r");
    int ch, i, length = 0;

    if (ferror(f))
        return;

    *num_lines = 0;
    while ((ch = fgetc(f)) != EOF && ! ferror(f))
        if (ch == '\n')
        {
            *num_lines += 1;
            *num_length = length > *num_length ? length : *num_length;
            length = 0;
        }
        else
            length++;
    fseek(f, 0, SEEK_SET);

    if (*num_lines == 0) (*num_lines)++;

    (*arr) = (char**)malloc(*num_lines * sizeof(char*));
    for (i = 0; i < *num_lines; i++)
    {
        (*arr)[i] = (char*)malloc(*num_length + 1 * sizeof(char));
        memset((*arr)[i], '\0', *num_length + 1);
    }

    i = -1;
    while ((++i < *num_lines) && fgets((*arr)[i], *num_length + 1, f))
        (*arr)[i][strcspn((*arr)[i], "\n")] = '\0';

    fclose(f);
}
