#!/usr/bin/env python3

import string
import sys

alpha = list(string.ascii_lowercase)

def react(str):
    reacted = str
    for i in alpha:
        reacted = reacted.replace(
                        "{}{}".format(i, i.upper()), ""
                    ).replace(
                        "{}{}".format(i.upper(), i), ""
                    )

    return reacted


def part1(str):

    polymer = str
    reacted = polymer

    reacted = react(str)
    while reacted != polymer:
        polymer = reacted
        reacted = react(polymer)

    print("No. of Units: {}".format(len(reacted)))

def part2(str):
    alpha = list(string.ascii_lowercase)
    min = sys.maxsize

    for i in alpha:
        polymer = str
        reacted = polymer
        reacted = reacted.replace(i, "").replace(i.upper(), "")

        reacted = react(reacted)
        while reacted != polymer:
            polymer = reacted
            reacted = react(polymer)

        min = len(reacted) if len(reacted) < min else min

    print("Shortest polymer: {}".format(min))


def main():
    with open("input") as f:
        file = f.readline().strip()

    part1(file)
    part2(file)

main()
