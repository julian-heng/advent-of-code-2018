#!/usr/bin/env bash

read_node()
(
    i="$1"
    num_nodes="${nodes[$i]}"
    num_meta="${nodes[$((i + 1))]}"
    ((i += 2))

    for ((j = 0; j < num_nodes; j++)); do
        read -r i meta < <(read_node "$i")
    done

    ((sum_meta += meta))

    for ((j = 0; j < num_meta; j++)); do
        ((sum_meta += nodes[i + j]))
    done

    printf "%d " "$((i + num_meta))" "${sum_meta}"
)

#read_node_2()
#(
#    node="$(($1 == 0 ? 1 : $1))"
#    i="$1"
#    num_nodes="${nodes[$i]}"
#    num_meta="${nodes[$((i + 1))]}"
#    sum_meta="0"
#    ((i += 2))
#
#    for ((j = 0; j < num_nodes; j++)); do
#        read -ra out < <(read_node_2 "$i")
#        i="${out[0]}"
#        meta_arr=("${out[@]:1}")
#        : "${meta_arr[@]}"
#    done
#
#    if ((num_nodes == 0)); then
#        for ((j = 0; j < num_meta; j++)); do
#            ((sum_meta += nodes[i + j]))
#        done
#    else
#        for ((j = 0; j < num_meta; j++)); do
#            : "${meta_arr[${nodes[$((i + j))]}]:=0}"
#            ((sum_meta += meta_arr[nodes[i + j]]))
#        done
#    fi
#
#    printf -v out "%d " "${meta_arr[@]:0:${node}}" "${sum_meta}" "${meta_arr[@]:$((node + 1))}"
#    printf "%s" "$((i + num_meta)) ${out}"
#)

part1()
(
    [[ -f "${PWD}/input" ]] && {
        read -ra nodes < "${PWD}/input"
        read -r _ sum_meta < <(read_node "0")
        printf "%d\\n" "${sum_meta}"
    }
)

main()
{
    part1
}

main
