#!/usr/bin/env python3

import re

def part1(line):
    def read(i):
        num_nodes = values[i]
        num_meta = values[i + 1]
        i += 2
        for j in range(num_nodes):
            i = read(i)
        for j in range(num_meta):
            sum_meta.append(values[i + j])
        return i + num_meta

    values = list(map(int, re.findall("\d+", line)))
    sum_meta = list()

    read(0)
    print(sum(sum_meta))

def part2(line):
    def read(i):
        num_nodes = values[i]
        num_meta = values[i + 1]
        i += 2
        meta = dict()
        this_meta = list()

        for j in range(num_nodes):
            i, last_meta = read(i)
            meta[j + 1] = last_meta

        for j in range(num_meta):
            this_meta.append(values[i + j])

        if num_nodes == 0:
            return i + num_meta, sum(this_meta)
        else:
            return i + num_meta, sum(meta.get(j, 0) for j in this_meta)

        return i + num_meta

    values = list(map(int, re.findall("\d+", line)))

    _, root = read(0)
    print(root)

def main():
    with open("input") as f:
        file = f.readline()

    part1(file)
    part2(file)

main()
