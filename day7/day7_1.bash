#!/usr/bin/env bash

trim()
{
    [[ "$*" ]] && {
        set -f
        set -- $*
        printf "%s" "$*"
        set +f
    }
}

part1()
{
    [[ -f "${PWD}/input" ]] && {
        mapfile -t file < "${PWD}/input"

        for line in "${file[@]}"; do
            read -r _ dependency _ _ _ _ _ target _ <<< "${line}"
            : "${steps[${dependency}]:="false"}"
            : "${steps[${target}]:="false"}"
            : "${depends[${dependency}]:=""}"
            : "${depends[${target}]:=""}"
            targets[${dependency}]+="${target} "
            depends[${target}]+="${dependency} "
        done

        for step in "${!steps[@]}"; do
            [[ ! "${targets[${step}]}" ]] && {
                last_step="${step}"
                break
            }
        done

        while [[ "$(trim "${depends[*]}")" ]]; do
            for step in "${!steps[@]}"; do
                [[ ! "${depends[${step}]}" && "${steps[${step}]}" != "true" ]] && {
                    done_steps+=("${step}")
                    steps[${step}]="true"
                    for next_step in "${!steps[@]}"; do
                        [[ "${step}" != "${next_step}" ]] && \
                            depends[${next_step}]="$(trim "${depends[${next_step}]//${step}}")"
                    done
                    break
                }
            done
        done

        done_steps+=("${last_step}")
        IFS="" order="${done_steps[*]}"
        printf "%s\\n" "${order}"
    }
}

main()
{
    declare -A steps
    declare -A depends
    declare -A targets

    part1
}

main
